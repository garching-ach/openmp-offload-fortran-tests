# README #

A small set of tests to evaluate the Fortran functionality of OpenMP offloading for polymorphic types and type bound procedures.


Fortran source files are located in src.

The systemfiles directory contains Makefiles and submission scripts for different systems and compilers. At present only marconi100 files exist.

The account variable in each submit.cmd script in the directories for each compiler must be changed to a valid account.

To run the tests set the environment variable ROOTDIR to the repository location including the repository itself

e.g.
export ROOTDIR=/home/joebloggs/openmp-offload-fortran-tests

create a run directory
e.g.
cd $ROOTDIR
mkdir rundir
cd rundir

and run the script runtests.sh
../runtests.sh

This will create directories for each set of tests and launch a job to run the tests. 

The output will appear as text in marconi100/<compiler>/out.<compiler>.<jobid> files.

huw.leggate@dcu.ie
