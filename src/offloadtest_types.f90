!> Contains subroutines to test the offloading capabilities
!> of OpenMP in the presence of polymorphic entities
module mod_offload_test
#ifdef USE_MPI
  use mpi
#endif

  implicit none

  !> The base class
  type :: particle_base
     real*8, dimension(3) :: x
     real*8, dimension(3) :: st
  end type particle_base

  !> An extension of the class
  type, extends(particle_base) :: particle_kinetic
    real*8, dimension(3) :: v
    integer*1            :: q
  end type particle_kinetic


contains

  subroutine copy_particle_kinetic(in, out)
    type(particle_kinetic), intent(in)  :: in
    type(particle_kinetic), intent(out) :: out
#ifdef OFFLOAD
    !$omp declare target
#endif
    out%x(1)  = in%x(1)
    out%x(2)  = in%x(2)
    out%x(3)  = in%x(3)
    out%st(1) = in%st(1)
    out%st(2) = in%st(2)    
  end subroutine copy_particle_kinetic

  !> Add one to all the elemsnts of the particle
  subroutine change_particle_kinetic(particle)
    type(particle_kinetic)  :: particle
#ifdef OFFLOAD
    !$omp declare target
#endif
    particle%x(1)  = particle%x(1) + 1.0
    particle%x(2)  = particle%x(2) + 1.0
    particle%x(3)  = particle%x(3) + 1.0
    particle%st(1) = particle%st(1) + 1.0
    particle%st(2) = particle%st(2) + 1.0
  end subroutine change_particle_kinetic
  

  !> Loop over particles and change each one
  subroutine particle_loop( n_particles, particles )
    integer, intent(in)                               :: n_particles
    class(particle_base), intent(inout), dimension(:) :: particles
    type(particle_kinetic)                            :: particle_tmp
    integer                                           :: i,j,nsteps
    real*8                                            :: start_time

    nsteps = 1000
    write(*,*) "Number of particles,steps=",n_particles,nsteps

    select type(p => particles)
    type is (particle_kinetic)
#ifdef OFFLOAD
       !$omp target map(tofrom:p) map(to:nsteps,n_particles)
       !$omp teams distribute parallel do private(i,j,particle_tmp)
#endif
       do i = 1, n_particles
          call copy_particle_kinetic(p(i),particle_tmp)
          do j = 1, nsteps
             call change_particle_kinetic(particle_tmp)
          enddo
          call copy_particle_kinetic(particle_tmp,p(i))
       end do
#ifdef OFFLOAD 
       !$omp end teams distribute parallel do
       !$omp end target
#endif
    end select
    
  end subroutine particle_loop

    
end module mod_offload_test


program offloadtest

  use mod_offload_test
#ifdef USE_MPI
  use mpi
#endif

  implicit none

  integer                                           :: n_particles, i
  real*8                                            :: start_time
  type(particle_kinetic),dimension(:), allocatable  :: particles

  n_particles = 1e6

#ifdef OFFLOAD
  write(*,*) "Running offloadtest with extended derived type."
#else
  write(*,*) "Running serial test with extended derived type."
#endif

  write(*,*) "OpenMP identifier", _OPENMP  

#ifdef USE_MPI
    start_time = MPI_Wtime()
#endif

  allocate(particles(n_particles))
  call particle_loop( n_particles, particles )
  deallocate(particles)
  
#ifdef USE_MPI
#ifdef OFFLOAD
  write(*,*) "Offloaded particle_loop time:", MPI_Wtime() - start_time
#else
  write(*,*) "Serial particle_loop time:", MPI_Wtime() - start_time
#endif
#endif
  
end program offloadtest
