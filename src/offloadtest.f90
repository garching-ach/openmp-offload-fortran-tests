program offloadtest

#ifdef USE_MPI
  use mpi
#endif

  integer                           :: n = 1000000000
  real*8, dimension(:),allocatable  :: array1, array2
  real*8                            :: val, start_time

  allocate(array1(n),array2(n))
  
#ifdef OFFLOAD
  write(*,*) "Running offloadtest. N=",N
#else
  write(*,*) "Running serial test. N=",N
#endif
  
  write(*,*) "OpenMP identifier", _OPENMP  

#ifdef USE_MPI
  start_time = MPI_Wtime()
#endif
  
  array1 = 1.0
  array2 = 2.0
  val = 2.0

#ifdef OFFLOAD
  !$omp target map(to:array1,val) map(tofrom:array2)    
  !$omp teams distribute parallel do private(i)
#endif
  do i=1,n
     array2(i) = array2(i) + val*array1(i)
  end do
#ifdef OFFLOAD
  !$omp end teams distribute parallel do
  !$omp end target
#endif

  write(*,*) 'Max error: ', maxval(abs(array2-4.0))    
#ifdef USE_MPI
#ifdef OFFLOAD
  write(*,*) "Offloaded loop time:", MPI_Wtime() - start_time
#else
  write(*,*) "Serial loop time:", MPI_Wtime() - start_time
#endif
#endif
  
  deallocate(array1,array2)

end program offloadtest
