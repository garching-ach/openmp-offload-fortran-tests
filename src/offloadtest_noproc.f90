!> Contains subroutines to test the offloading capabilities
!> of OpenMP in the presence of polymorphic entities
!> This version tests the use of type bound procedures
module mod_offload_test
#ifdef USE_MPI
  use mpi
#endif
  
  implicit none

  private

  public :: particle_base, particle_kinetic, particle_loop, push_boris_particle_kinetic
  
  !> The base class
  type, abstract :: particle_base
     real*8, dimension(3) :: x
     real*8, dimension(3) :: st
     real*8               :: m
  end type particle_base

  !> An extension of the class
  type, extends(particle_base) :: particle_kinetic
     real*8, dimension(3) :: v
     integer*1            :: q
  end type particle_kinetic
 
contains

  subroutine copy_particle_kinetic(in, out)
    type(particle_kinetic), intent(in)  :: in
    type(particle_kinetic), intent(out) :: out
#ifdef OFFLOAD
    !$omp declare target
#endif
    out%x(1)  = in%x(1)
    out%x(2)  = in%x(2)
    out%x(3)  = in%x(3)
    out%st(1) = in%st(1)
    out%st(2) = in%st(2)    
  end subroutine copy_particle_kinetic

  !> Add one to all the elements of the particle
  subroutine change_particle_kinetic(particle)
    type(particle_kinetic)  :: particle
#ifdef OFFLOAD
    !$omp declare target
#endif
    particle%x(1)  = particle%x(1) + 1.0
    particle%x(2)  = particle%x(2) + 1.0
    particle%x(3)  = particle%x(3) + 1.0
    particle%st(1) = particle%st(1) + 1.0
    particle%st(2) = particle%st(2) + 1.0
  end subroutine change_particle_kinetic

  subroutine push_boris_particle_kinetic(particle,E,B,dt)
    type(particle_kinetic), intent(inout)  :: particle
    real*8, intent(in), dimension(3)       :: E,B
    real*8, intent(in)                     :: dt
    real*8  :: B2, Bnorm, R, Rphi, fE, fB, eom
    
#ifdef OFFLOAD
  !$omp declare target
#endif

    eom = 1.602176565d-19 / (particle%m * 1.660539040d-27)
    B2    = dot_product(B,B)
    Bnorm = sqrt(B2)

    fE =     particle%q*eom * dt * 0.5d0
    fB = tan(particle%q*eom * dt * 0.5d0 * Bnorm) / Bnorm

    ! Calculate the electric field update (v^n-1/2 -> v-) with the Boris method
    particle%v = particle%v + fE * E
    ! Calculate the rotation
    particle%v = (particle%v + 2.d0*fB/(1.d0+fB*fB*B2)*( &
         cross_product(particle%v,B) &
         - fB * particle%v * B2 &
         + fB * B * dot_product(particle%v,B)))
    ! Calculate the next electric field update (v+ -> v^n+1/2)
    particle%v = particle%v + fE * E
    ! update the position from v^n to v^(n+1)
    ! Calculate the new R and RPhi
    R    = particle%x(1) + particle%v(1) * dt
    RPhi = particle%v(3) * dt

    ! Calculate the new R, Phi, Z
    particle%x(1) = sqrt(R**2 + RPhi**2)
    particle%x(2) = particle%x(2) + dt * particle%v(2)
    particle%x(3) = particle%x(3) + asin(RPhi / particle%x(1))
    
    ! Adjust R and Phi velocities (component 1 and 3) to the new reference frame
    particle%v(1:3:2) = [R     * particle%v(1) + RPhi * particle%v(3), &
         -RPhi * particle%v(1) + R    * particle%v(3)] / particle%x(1)

  end subroutine push_boris_particle_kinetic
  
  pure function cross_product(a, b)
    real*8, dimension(3) :: cross_product
    real*8, dimension(3), intent(in) :: a, b

#ifdef OFFLOAD
    !$omp declare target
#endif

    cross_product(1) = a(2) * b(3) - a(3) * b(2)
    cross_product(2) = a(3) * b(1) - a(1) * b(3)
    cross_product(3) = a(1) * b(2) - a(2) * b(1)

  end function cross_product

  !> Loop over particles and change each one
  subroutine particle_loop( n_particles, particles )
    integer, intent(in)                               :: n_particles
    class(particle_base), intent(inout), dimension(:) :: particles
    type(particle_kinetic)                            :: particle_tmp
    integer                                           :: i,j,nsteps
    real*8                                            :: start_time
    real*8, dimension(3)                              :: E, B
    real*8                                            :: dt

    nsteps = 1000
    write(*,*) "Number of particles,steps=",n_particles,nsteps
    dt = 1.0d-10
    E = [0.1,0.2,-0.1]
    B = [-0.1,0.1,0.2]

#ifdef USE_MPI
    start_time = MPI_Wtime()
#endif

    select type(p => particles)
    type is (particle_kinetic)
#ifdef OFFLOAD
       !$omp target map(tofrom:p) map(to:nsteps,n_particles)
       !$omp teams distribute parallel do private(i,j,particle_tmp)
#endif
       do i = 1, n_particles
          call copy_particle_kinetic(p(i),particle_tmp)
          do j = 1, nsteps
             call push_boris_particle_kinetic(particle_tmp,E,B,dt)
          enddo
          call copy_particle_kinetic(particle_tmp,p(i))
       end do
#ifdef OFFLOAD 
       !$omp end teams distribute parallel do
       !$omp end target
#endif
    end select
  
  end subroutine particle_loop

end module mod_offload_test


program offloadtest

  use mod_offload_test
#ifdef USE_MPI
  use mpi
#endif

  implicit none

  integer                                           :: n_particles, i
  real*8                                            :: start_time
  type(particle_kinetic),dimension(:), allocatable  :: particles
  
  n_particles = 1e6
  
#ifdef OFFLOAD
  write(*,*) "Running offloadtest with explicit subroutine."
#else
  write(*,*) "Running serial test with explicit subroutine."
#endif

  write(*,*) "OpenMP identifier", _OPENMP  

#ifdef USE_MPI
  start_time = MPI_Wtime()
#endif

  allocate(particles(n_particles))
  call particle_loop( n_particles, particles )
  deallocate(particles)
  
#ifdef USE_MPI
#ifdef OFFLOAD
  write(*,*) "Offloaded particle_loop time:", MPI_Wtime() - start_time
#else
  write(*,*) "Serial particle_loop time:", MPI_Wtime() - start_time
#endif
#endif
  
end program offloadtest
