#!/bin/bash

# Script to run a series of openmp offloadtests
# currently only runs marconi100 tests

run_test()
{
    mkdir $1
    cd $1
    cp $SYSTEMDIR/$1/Makefile .
    sbatch $SYSTEMDIR/$1/submit.cmd  
    cd ..
}

export SRCDIR=$ROOTDIR/src
export SYSTEMDIR=$ROOTDIR/systemfiles/marconi100
export SCRIPTDIR=$SYSTEMDIR/scripts

mkdir marconi100
cd marconi100

run_test gcc-8.4
run_test gcc-10.3
run_test gcc-11.2
run_test hpc-sdk-2022

cd ..


