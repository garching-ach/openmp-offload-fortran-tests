#!/bin/bash
#SBATCH -A FUSIO_HLST_3
#SBATCH -p m100_fua_prod
#SBATCH --qos m100_qos_fuadbg
#SBATCH --time 02:00:00  
#SBATCH -N 1                
#SBATCH --ntasks-per-node=1 # out of 128
#SBATCH --cpus-per-task=128
#SBATCH --gres=gpu:1        # out of 4
#SBATCH --mem=160000         # memory per node out of 246000MB
#SBATCH --job-name=hpc-sdk-2022
#SBATCH --output=out.%x.%j

module purge
module list
module load profile/candidate hpc-sdk/2022--binary spectrum_mpi/10.4.0--binary cuda
###module load profile/candidate hpc-sdk/2022--binary cuda

mpipgifort --show
###mpif90 --show
mpirun --version

export OMP_PROC_BIND=true
export OMP_PLACES=threads 
export OMP_NUM_THREADS=128
export CUDA_VISIBLE_DEVICES=${SLURM_LOCALID}

${SCRIPTDIR}/runtest.sh
