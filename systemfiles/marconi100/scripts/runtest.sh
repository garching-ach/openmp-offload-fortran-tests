#!/bin/bash

run_one_test()
{

	
   make clean
   make SRC=$1
   retval=$?
   if [ "$retval" == 0 ]
   then
      rm -rf /tmp/nvidia
      ln -s $TMPDIR /tmp/nvidia
      mpirun  -gpu --map-by socket:PE=16 ./offload
      rm -rf /tmp/nvidia
   else
      echo "Make error. Tests aborted"
      exit	
   fi

   make clean
   make offload SRC=$1

   retval=$?
   if [ "$retval" == 0 ]
   then  
      rm -rf /tmp/nvidia
      ln -s $TMPDIR /tmp/nvidia
      echo "Running $1"
      mpirun -gpu --map-by socket:PE=16 ./offload
      rm -rf /tmp/nvidia
   else
      echo "Make error. Offload test not run"
   fi
}


run_one_test offloadtest.f90
run_one_test offloadtest_types.f90
run_one_test offloadtest_noproc.f90
run_one_test offloadtest_procedures.f90
run_one_test offloadtest_defproc.f90
#run_one_test offloadtest_select.f90


